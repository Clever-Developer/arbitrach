$(document).ready(function () {

    $('table.tabulka_kariet').DataTable({
        "pagingType": "simple_numbers",
        "processing": true,
        "responsive": true,
        "lengthChange": false,
        "language": {
            "zeroRecords": "Ничего не найдено",
            "loadingRecords": "Загрузка...",
            "info": "Страница _PAGE_ из _PAGES_",
            "infoEmpty": "",
            "search": "<b>Поиск:</b> ",
            "infoFiltered": "(Найдено записей: _TOTAL_)",
            "paginate": {
                "first": "<button class='btn btn-primary' style='margin-right:10px'>В Начало</button>",
                "next": "<i class='fa fa-toggle-right' style='font-size:25px'></i>",
                "previous": "<i class='fa fa-toggle-left' style='font-size:25px'></i>",
                "last": "<button class='btn btn-primary' style='margin-left:10px'>В Конец</button>",
                "numbers": "<button class='btn btn-danger'>_NUMBER_</button>"
            }
        },
        "order": [],
        "columnDefs": [ {
            "targets"  : 'no-sort',
            "orderable": false,
        }],
        "aaSorting": [
            [0, "desc"]
        ],
        "iDisplayLength": 5
    });
});

/*
* айди изменяемой модели
* айди связной модели
* айди иконки прогрузки ответа
* урл к экшену на обработку, переданный из контроллера
* this
* */
function CheckSwitch(first_id, last_id, spinner_sel, changesUrl, elem) {
    var param;

    if (elem.checked) {
        param = true;
    } else {
        param = false;
    }

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: changesUrl,
        type: "POST",
        data: {f_id: first_id, l_id: last_id, parametr: param},
        global: false,
        beforeSend: function () {
            $("#" + spinner_sel).show();
        },
        complete: function () {
            $("#" + spinner_sel).hide();
        },
        //success: function() {},
        error: function (ts) {
            alert(ts.responseText);
        }
    });
}
