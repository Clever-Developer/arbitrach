$(document).ready(function () {
    function toggle_show(id) {
        document.getElementById(id).style.display = document.getElementById(id).style.display == 'none' ? 'block' : 'none';
    }


    if (document.getElementsByClassName('show-button').length > 0) {
        var buttonsList = document.getElementsByClassName('show-button');

        for (var i = 0; i < buttonsList.length; i++) {
            buttonsList[i].addEventListener('click', function () {
                if (this.parentNode.classList.contains('show')) {
                    this.parentNode.classList.remove('show');
                } else {
                    this.parentNode.classList.add('show');
                }

            })
        }

    }


    if (document.getElementById('datepicker')) {
        $("#datepicker").datepicker({
            numberOfMonths: 2,
            changeYear: true,
            showOtherMonths: true,
            onChangeMonthYear: function () {
                addRangeToDatepicker($.datepicker._curInst.selectedYear);
            },
            onSelect: function () {
                addRangeToDatepicker($.datepicker._curInst.selectedYear);
            },
            beforeShow: function () {
                addRangeToDatepicker($.datepicker._curInst ? $.datepicker._curInst.selectedYear : new Date().getFullYear());
                $('.datepicker-background').show();
            },
            onClose: function () {
                $('.datepicker-background').hide();
            }
        });
        $.datepicker.regional["ru"];

        $('#ui-datepicker-div').find('select').val('2016').trigger("change");


        setTimeout(function () {
            // addRangeToDatepicker()
        }, 1000);

        var yearsCount = 6;

        function newYearSelect() {

            var currentYear = new Date().getFullYear();
            if ($.datepicker._curInst) {
                if ($.datepicker._curInst.selectedYear > new Date().getFullYear()) {
                    currentYear = $.datepicker._curInst.selectedYear;
                } else if ($.datepicker._curInst.selectedYear < new Date().getFullYear() - 5) {
                    currentYear = $.datepicker._curInst.selectedYear + 5;

                }
            }
            var list = '';

            for (var i = yearsCount - 1; i > -1; i--) {
                var value = currentYear - i;
                var calendarYear = $.datepicker._curInst ? $.datepicker._curInst.selectedYear : new Date().getFullYear();
                var isActive = value == calendarYear;
                list += ('<li class="' + (isActive ? 'active' : '') + '" year="' + value + '">' + value + '</li>');
            }
            var listFin = "<ul > " + list + "</ul>";

            var range = '<div id="slider"><div id="custom-handle" class="ui-slider-handle"></div></div>';
            return '<div class="datepicker-years">' + range + listFin + '</div>';
        }

        function addRangeToDatepicker(value) {
            setTimeout(function () {
                console.log(value);

                $('#ui-datepicker-div').append(newYearSelect());

                var handle = $("#custom-handle");
                $("#slider").slider({
                    min: new Date().getFullYear() - yearsCount + 1,
                    max: new Date().getFullYear(),
                    value: value || new Date().getFullYear(),
                    change: function (event, ui) {
                        $('#ui-datepicker-div').find('select').val(ui.value).trigger("change");
                    }
                });
                $(".datepicker-years li").click(function () {
                    $('#ui-datepicker-div').find('select').val($(this).attr('year')).trigger("change");
                });
            }, 0);
        }

        $('.datepicker-background').click(function () {
            $('.datepicker-background').hide();
            $("#ui-datepicker-div").datepicker("hide");
        })
    }

    if (document.getElementById('range1') && document.getElementById('range2') && document.getElementById('range3') && document.getElementById('range4')) {


        $("#range1, #range2, #range3, #range4").slider({
            orientation: "horizontal",
            range: "min",
            max: 100,
            value: 50,
            change: function (event, ui) {
                $('#' + ui.handle.parentElement.id + 'Result').text($(this).slider("value"));
            },
            slide: function (event, ui) {
                $('#' + ui.handle.parentElement.id + 'Result').text($(this).slider("value"));
            }
        });
    }
    if (document.getElementById('range4')) {

        $(" #range4").slider({
            orientation: "horizontal",
            range: "min",
            min: 1,
            max: 5,
            value: 3,
            change: function (event, ui) {
                $('#' + ui.handle.parentElement.id + 'Result').text($(this).slider("value"));
            },
            slide: function (event, ui) {
                $('#' + ui.handle.parentElement.id + 'Result').text($(this).slider("value"));
            }
        });
    }
});

