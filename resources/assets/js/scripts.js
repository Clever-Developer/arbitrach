function checkTime() {
    let local_hours = new Date().getHours();
    let user_night = $('#user-night-mode').val();
    let user_auth = $('#user-auth').val();

    if (user_auth === '1' && getCookie('disable-time-modal') !== 'true' && local_hours >= 18 && user_night !== '1') {
        var retVal = confirm("Уже ночь. Включить ночной режим?");
        if (retVal === true) {
            axios.put('/users/update', {is_night_mode: true})
                .then(
                    response => {
                        setCookie('disable-time-modal', 'true', 1);
                        location.reload();
                    }
                )
                .catch(error => {
                    console.log(error);
                })
        } else {
            setCookie('disable-time-modal', 'true', 1);
        }
    }

}

$(document).ready(function () {
    checkTime();
});

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}