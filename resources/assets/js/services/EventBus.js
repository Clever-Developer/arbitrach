export const EventBus = new Vue({
    data: {
        displayedPost: 0,
        postIndex: 0
    },

    methods: {
        setDisplayedPost(id) {
            this.displayedPost = id;
        },

        setIndex(index) {
            this.postIndex = index;
        },

        addPostLike() {
            this.$emit('postLiked', this.displayedPost);
        },

        delPostLike() {
            this.$emit('postDisliked', this.displayedPost);
        },

        prevPost() {
            if (this.$scrollTo('#post-' + (this.postIndex - 1), 500, {easing: 'ease', cancelable: false})) {
                this.postIndex--
            }
        },

        nextPost() {
            if (this.$scrollTo('#post-' + (this.postIndex + 1), 500, {easing: 'ease', cancelable: false})) {
                this.postIndex++
            }
        },

        topPost() {
            this.$scrollTo('#post-' + this.postIndex, 500, {easing: 'ease', cancelable: false})
        },

        openComments() {
            this.$emit('showPost', this.displayedPost);
        },

        closeComments() {
            this.$emit('closePost');
        },

        toggleCollapse() {
            this.$emit('collapsePost', this.postIndex);
        }
    }
});