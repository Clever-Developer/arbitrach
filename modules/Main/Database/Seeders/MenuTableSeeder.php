<?php

namespace Modules\Main\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Main\Entities\Menu;
use Modules\Post\Entities\Category;

class MenuTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Menu::truncate();

        $menus = collect([
            'Лучшее'            => 'best',
            'Свежее'            => 'new',
            'Новости партнерок' => 'partners',
            'Сообщества'        => 'communities'
        ]);

        $idx = 1;

        foreach ($menus as $k => $menu) {
            $menu = Menu::create([
                'title'  => $k,
                'slug'   => $menu,
                'active' => true,
                'order'  => $idx++
            ]);

            if ($menu->slug === 'communities') {
                continue;
            }

            $menu->categories()->saveMany(Category::all());
        }
    }
}
