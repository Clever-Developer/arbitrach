<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Main\Http\Controllers'], function () {
    Route::group(['prefix' => 'communities', 'as' => 'communities.'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::post('/', 'CommunityController@store');
            Route::get('/mine', 'CommunityController@mine')->name('mine');
            Route::get('/create', 'CommunityController@create')->name('create');

        });

        Route::get('/', 'CommunityController@index')->name('index');
        Route::get('/all', 'CommunityController@all')->name('all');
        Route::post('/filter', 'CommunityController@filter');
        Route::get('/{community}/{filter?}', 'CommunityController@show')->name('show');
    });

    Route::match(['get', 'post'], '/{menu?}/{category?}', 'MainController@index')->name('index');
});
