@extends('main::layouts.master')

@section('content')

    @include('main::communities.partials.navigation')

    <community-filter mode="mine"></community-filter>

    <div id="communities-list">
        @include('main::communities.partials.list')
    </div>

@endsection