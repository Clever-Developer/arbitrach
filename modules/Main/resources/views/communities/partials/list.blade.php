@foreach($communities as $community)
    <div class="following">
        <a href="{{ route('communities.show', [$community->slug]) }}">
            <img src="{{ $community->image }}" alt="">
        </a>
        <div class="community-wrapper">
            <div class="following-item">
                <a href="{{ route('communities.show', [$community->slug]) }}">
                    {{ $community->name }}
                </a>
            </div>
            <div class="following-item">
                <span><b>{{ $community->posts_count }}</b> постов</span>
                <span>.</span>
                <span><b>{{ $community->users_count }}</b> подписчиков</span>
            </div>
            <div class="following-item" style="display: inline-block">
                @foreach($community->tags as $tag)
                    <a href="">{{ $tag->name }}</a>
                @endforeach
            </div>
            @auth
                <subscribe-community-button
                        subscribed="{{ in_array($community->id, Auth::user()->communities->pluck('id')->toArray()) }}"
                        :id="{{ $community->id }}"
                ></subscribe-community-button>
            @endauth
        </div>
    </div>
@endforeach