@extends('main::layouts.master')

@section('content')
    <div class="personal-room">
        <div class="personal-room-header">
            <img src="{{ asset($community->image) }}" alt="personal photo">
            <div class="personal-room-title">
                <span onclick="location.href = '{{ route('cabinet.index') }}'" style="cursor: pointer">
                    {{ $community->name }}
                </span>
            </div>
        </div>
        <div class="personal-room-photo"><img src="{{ asset('image/img-title.png') }}" alt="title"></div>
    </div>
    <div class="personal-statistic">
        <div><span></span><b>{{ count($community->posts) }}</b> постов</div>
        <div><span></span><b>{{ count($community->users) }}</b> подписчиков</div>
        @auth
            <subscribe-community-button
                    subscribed="{{ in_array($community->id, Auth::user()->communities->pluck('id')->toArray()) }}"
                    :id="{{ $community->id }}"
            ></subscribe-community-button>
        @endauth
    </div>
    <nav class="navbar navbar-expand-lg">
        <div class="collapse navbar-collapse" id="navbarNav2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ (request('filter') === 'hot' || !request('filter')) ? 'active' : '' }}"
                       href="{{ route('communities.show', [$community->slug, 'hot']) }}" title="Горячее"
                       style="text-align: center">Горячее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request('filter') === 'best' ? 'active' : '' }}"
                       href="{{ route('communities.show', [$community->slug, 'best']) }}" title="Лучшее">Лучшее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request('filter') === 'new' ? 'active' : '' }}"
                       href="{{ route('communities.show', [$community->slug, 'new']) }}"
                       title="Мои сообщества">Свежее</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request('filter') === 'search' ? 'active' : '' }}"
                       href="{{ route('communities.show', [$community->slug, 'search']) }}"
                       title="Поиск">Поиск</a>
                </li>
            </ul>
        </div>
    </nav>

    <post-list api-url="{{ route('api.communities.show', [$community->slug, request('filter')]) }}">
        @if(request('filter') === 'search')
            <community-post-filter style="margin-bottom: 20px" slot="filter"></community-post-filter>
        @endif
    </post-list>

@endsection