<!doctype html>

<html lang="ru">
<head>
    <meta charset="utf-8">

    <title>Arbitrach</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tags.css') }}">
    <link href="{{ asset('assets/vendors/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">
    @stack('css')
</head>

<body @if(auth()->check() && auth()->user()->is_night_mode) class="night" @endif>
@if(auth()->check()) <input type="hidden" id="user-night-mode" value="{{ auth()->user()->is_night_mode }}"> @endif
@if(auth()->check()) <input type="hidden" id="user-auth" value="1"> @endif

<div id="app">
    @include('main::sections.header')

    <section class="main-container">
        <div class="container-left">
            <div class="auth-component-mob">
                @guest
                    <auth></auth>
                @endguest
            </div>
            @yield('content')
        </div>
        @include('main::sections.right-bar')
    </section>
</div>

@include('main::sections.footer')

@include('main::sections.scripts')

@stack('js')

</body>
</html>
