<footer>
    <div class="footer-container">
        <div class="footer-info">
            <h6>Информация</h6>
            <ul>
                <li><a href="#">Контакты</a></li>
                <li><a href="#">Ошибки</a></li>
                <li><a href="#">Предложения</a></li>
                <li><a href="#">Правила</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">Бан</a></li>
            </ul>
        </div>
        <div class="footer-vk">
            <a href="#"><img src="{{ asset('image/vk.png') }}"></a>
        </div>
        <div class="footer-twitter">
            <a href="#"><img src="{{ asset('image/twitter.png') }}"></a>
        </div>
        <div class="footer-info">
            <h6>Партнеры</h6>
            <ul>
                <li><a href="#">pornhub.com</a></li>
                <li><a href="#">apple.com</a></li>
                <li><a href="#">tesla.com</a></li>
                <li><a href="#">design.com</a></li>
            </ul>
        </div>
        <div class="big-logo">
            <a href="#"></a>
        </div>
    </div>
    <hr>
</footer>