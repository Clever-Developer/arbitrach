<?php

namespace Modules\Api\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Post\Http\Resources\PostResource;

class LikeResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'user_id'      => $this->user_id,
            'vote'         => $this->vote,
            'post' => $this->whenLoaded('preparedPost', function () {
                return PostResource::make($this->preparedPost);
            })
        ];
    }
}
