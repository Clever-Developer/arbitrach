<?php

namespace Modules\Api\Http\Resources;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Post\Entities\Tag;
use Modules\Users\Entities\User;

class SearchResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource instanceof Model) {
            return [
                'name'  => $this->when($this->resource instanceof Model, $this->name),
                'show'  => $this->when($this->resource instanceof Model, $this->show),
                'count' => $this->when($this->resource instanceof Tag, $this->posts_count),
                'image' => $this->when(! $this->resource instanceof Tag, function () {
                    if ($this->resource instanceof User) {
                        return asset($this->avatar);
                    } else {
                        return asset($this->image);
                    }
                }),
            ];
        } else {
            return [
                'title' => $this->when(isset($this['title']), $this['title']),
                'type'  => $this->when(isset($this['type']), $this['type']),
            ];
        }
    }
}
