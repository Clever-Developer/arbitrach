<?php

namespace Modules\Api\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Modules\Users\Entities\User;
use Modules\Users\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => $data['password'],
            'avatar'   => 'public/users/default_avatar.svg'
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->create($request->all());

        \Auth::login($user);

        return response()->json(['success' => true]);
    }
}
