<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Entities\Comment;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Resources\CommentResource;
use Modules\Post\Http\Resources\PostResource;

class SaveController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPosts(Request $request)
    {
        $posts = \Auth::user()->savedPosts()->prepared();

        if ($filter = $request->get('filter', false)) {
            if ($search = array_get($filter, 'search', false)) {
                $posts = $posts->where('title', 'like', "%$search%");
            }
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getComments(Request $request)
    {
        $comments = \Auth::user()->savedComments()->with(['post', 'user']);

        if ($filter = $request->get('filter', false)) {
            if ($search = array_get($filter, 'search', false)) {
                $comments = $comments->where('text', 'like', "%$search%");
            }
        }

        $comments = $comments->paginate(10);

        return CommentResource::collection($comments);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSave(Request $request)
    {
        $type = $request->get('type') == 1 ? Post::class : Comment::class;
        $id   = $request->get('id');

        if ($type === Post::class) {
            \Auth::user()->savedPosts()->attach($id);
        }

        if ($type === Comment::class) {
            \Auth::user()->savedComments()->attach($id);
        }

        return response()->json(['status' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSave(Request $request)
    {
        $type = $request->get('type') == 1 ? Post::class : Comment::class;
        $id   = $request->get('id');

        if ($type === Post::class) {
            \Auth::user()->savedPosts()->detach($id);
        }

        if ($type === Comment::class) {
            \Auth::user()->savedComments()->detach($id);
        }

        return response()->json(['status' => true]);
    }
}
