<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Post\Entities\Tag;
use Modules\Post\Http\Resources\PostResource;
use Modules\Post\Http\Resources\TagResource;

class TagController extends ApiController
{

    /**
     * @param Tag $tag
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function posts(Tag $tag, Request $request)
    {
        $posts = $tag->posts()->prepared();

        if (\Auth::check()) {
            $posts = $posts->withoutIgnoredUsers()->withoutIgnoredTags();
        }

        if ($filter = $request->get('filter', false)) {
            if ($search = array_get($filter, 'search', false)) {
                $posts = $posts->where('title', 'like', "%$search%");
            }

            if ($tags = array_get($filter, 'tags', false)) {
                if (! empty($tags)) {
                    foreach ($tags as $tag) {
                        $posts = $posts->whereHas('tags', function ($q) use ($tag) {
                            $q->where('tag_id', $tag['id']);
                        });
                    }
                }
            }

            if ($user = array_get($filter, 'user', false)) {
                $posts = $posts->where('user_id', $user['id']);
            }

            if ($community = array_get($filter, 'community', false)) {
                $posts = $posts->where('community_id', $community['id']);
            }

            if ($rating = array_get($filter, 'rating', false)) {
                $posts = $posts->where('rating', '>=', $rating);
            }
        }

        $posts = $posts->paginate($this->paginate);

        return PostResource::collection($posts);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $tags = Tag::prepared();

        if ($search = $request->get('search', false)) {
            $tags = $tags->where('name', 'like', "%$search%");
        }

        $tags = $tags->get();

        return TagResource::collection($tags);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubscribedTags()
    {
        $subs_tags = \Auth::user()->subscribedTags()->where('ignored', false)->get();
        $tags      = Tag::whereNotIn('id', $subs_tags->pluck('id'))->get();

        $subs_tags = TagResource::collection($subs_tags)->jsonSerialize();
        $tags      = TagResource::collection($tags)->jsonSerialize();

        return response()->json(compact('subs_tags', 'tags'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIgnoredTags()
    {
        $ignored_tags = \Auth::user()->subscribedTags()->where('ignored', true)->get();
        $tags         = Tag::whereNotIn('id', $ignored_tags->pluck('id'))->get();

        $ignored_tags = TagResource::collection($ignored_tags)->jsonSerialize();
        $tags         = TagResource::collection($tags)->jsonSerialize();

        return response()->json(compact('ignored_tags', 'tags'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribeTag(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedTags()->attach($id, ['ignored' => false]);

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unsubscribeTag(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedTags()->detach($id);

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ignoreTag(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedTags()->attach($id, ['ignored' => true]);

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unignoreTag(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedTags()->detach($id);

        return response()->json([
            'status' => true
        ]);
    }
}
