<?php

namespace Modules\Api\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Users\Entities\User;
use Modules\Users\Http\Resources\UserResource;

class SubscribeController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubscribedUsers()
    {
        $subscribers = \Auth::user()->subscribedUsers()
            ->where('ignored', false)
            ->get();

        $user_list = User::whereNotIn('id',
            array_merge($subscribers->pluck('id')->toArray(), [\Auth::id()]))->get(['name', 'id', 'avatar']);

        $subscribers = UserResource::collection($subscribers)->jsonSerialize();
        $user_list   = UserResource::collection($user_list)->jsonSerialize();

        return response()->json(compact('subscribers', 'user_list'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getIgnoredUsers()
    {
        $ignored = \Auth::user()->subscribedUsers()
            ->where('ignored', true)
            ->get();

        $user_list = User::whereNotIn('id', array_merge($ignored->pluck('id')->toArray(), [\Auth::id()]))->get();

        $ignored   = UserResource::collection($ignored)->jsonSerialize();
        $user_list = UserResource::collection($user_list)->jsonSerialize();

        return response()->json(compact('ignored', 'user_list'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribeUser(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedUsers()->attach($id, ['ignored' => false]);

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ignoreUser(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedUsers()->attach($id, ['ignored' => true]);

        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unsubscribeUser(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedUsers()->detach($id);

        return response()->json(['status' => true]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unignoreUser(Request $request)
    {
        $id = $request->get('id');

        \Auth::user()->subscribedUsers()->detach($id);

        return response()->json(['status' => true]);
    }
}
