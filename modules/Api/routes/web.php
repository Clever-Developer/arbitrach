<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Api\Http\Controllers'], function () {
    Route::group(['prefix' => 'api', 'as' => 'api.'], function () {
        // search
        Route::post('search', 'SearchController@search');

        // viewed post
        Route::post('users/posts/viewed', 'UserController@postViewed');

        // auth
        Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
            Route::post('login', 'Auth\LoginController@login')->name('login');
            Route::post('register', 'Auth\RegisterController@register')->name('register');
            Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        });

        // posts
        Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {
            Route::post('/', 'PostController@index')->name('index');

            Route::group(['middleware' => 'auth'], function () {
                Route::post('/my', 'PostController@my')->name('my');
            });
        });

        // comments
        Route::group(['prefix' => 'comments', 'as' => 'comments.'], function () {
            Route::post('post', 'CommentController@postComments')->name('index');

            Route::group(['middleware' => 'auth'], function () {
                Route::post('/image-preview', 'CommentController@imagePreview');
                Route::post('/', 'CommentController@store');
                Route::put('/{comment}', 'CommentController@update');
                Route::delete('/{comment}', 'CommentController@destroy');
                Route::post('search', 'CommentController@search');
                Route::post('saves', 'CommentController@savedComments')->name('saves');
                Route::post('my', 'CommentController@myComments')->name('my');
                Route::post('answers', 'CommentController@getAnswers')->name('answers');
            });
        });

        // communities
        Route::group(['prefix' => 'communities', 'as' => 'communities.'], function () {
            Route::group(['middleware' => 'auth'], function () {
                Route::get('subscribed', 'CommunityController@getSubscribedCommunities');
                Route::get('ignored', 'CommunityController@getIgnoredCommunities');
                Route::post('subscribe', 'CommunityController@subscribe');
                Route::post('unsubscribe', 'CommunityController@unsubscribe');
                Route::post('ignore', 'CommunityController@ignore');
                Route::post('unignore', 'CommunityController@unignore');
            });

            Route::post('/posts', 'CommunityController@getPosts')->name('posts');
            Route::post('/posts', 'CommunityController@getPosts')->name('posts');
            Route::post('/search', 'CommunityController@search');
            Route::post('/{community}/{filter?}', 'CommunityController@show')->name('show');
        });

        Route::group(['middleware' => 'auth'], function () {

            // likes
            Route::group(['prefix' => 'likes', 'as' => 'likes.'], function () {
                Route::group(['prefix' => 'posts'], function () {
                    Route::post('/', 'LikeController@posts')->name('posts');
                    Route::post('like', 'LikeController@likePost');
                    Route::post('dislike', 'LikeController@dislikePost');
                });

                Route::group(['prefix' => 'comments'], function () {
                    Route::post('like', 'LikeController@likeComment');
                    Route::post('dislike', 'LikeController@dislikeComment');
                });
            });

            // tags
            Route::group(['prefix' => 'tags', 'as' => 'tags.'], function () {
                Route::post('search', 'TagController@search');
                Route::get('subscribed', 'TagController@getSubscribedTags');
                Route::post('subscribe', 'TagController@subscribeTag');
                Route::post('unsubscribe', 'TagController@unsubscribeTag');
                Route::get('ignored', 'TagController@getIgnoredTags');
                Route::post('ignore', 'TagController@ignoreTag');
                Route::post('unignore', 'TagController@unignoreTag');
                Route::post('{tag}/posts', 'TagController@posts')->name('posts');
            });

            // subscribes
            Route::group(['prefix' => 'subscribes'], function () {
                Route::get('subscribed', 'SubscribeController@getSubscribedUsers');
                Route::post('subscribe', 'SubscribeController@subscribeUser');
                Route::post('unsubscribe', 'SubscribeController@unsubscribeUser');

                Route::get('ignored', 'SubscribeController@getIgnoredUsers');
                Route::post('ignore', 'SubscribeController@ignoreUser');
                Route::post('unignore', 'SubscribeController@unignoreUser');
            });

            // saves
            Route::group(['prefix' => 'saves', 'as' => 'saves.'], function () {
                Route::post('posts', 'SaveController@getPosts')->name('posts');
                Route::post('comments', 'SaveController@getComments')->name('comments');
                Route::post('add', 'SaveController@createSave');
                Route::post('delete', 'SaveController@deleteSave');
            });

            // notes
            Route::group(['prefix' => 'notes', 'as' => 'notes.'], function () {
                Route::post('/', 'NoteController@index')->name('index');
                Route::post('/add', 'NoteController@store');
                Route::get('/{user}', 'NoteController@user');
                Route::delete('/{note}', 'NoteController@delete');
            });

            // users
            Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
                Route::group(['prefix' => 'posts'], function () {
                    Route::group(['prefix' => 'state'], function () {
                        Route::get('{type}', 'UserController@getPostsState');
                        Route::post('{type}', 'UserController@changePostsState');
                    });
                    Route::post('{sort?}', 'UserController@posts')->name('posts');
                });
                Route::post('show/{user}/{sort?}', 'UserController@show')->name('show');
                Route::get('/', 'UserController@getUser');
                Route::post('/change', 'UserController@changeUser');
            });
        });
    });

});
