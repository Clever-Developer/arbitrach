<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CabinetController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $plus  = \Auth::user()->likes()->count();
        $minus = \Auth::user()->dislikes()->count();
        $user  = \Auth::user();

        return view('users::cabinet.index', compact('plus', 'minus', 'user'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function settings()
    {
        $user  = \Auth::user();
        $plus  = $user->likes()->count();
        $minus = $user->dislikes()->count();

        return view('users::cabinet.settings', compact('plus', 'minus', 'user'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function votes()
    {
        return view('users::cabinet.votes');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function answers()
    {
        return view('users::cabinet.answers');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subscriptions()
    {
        return view('users::cabinet.subscriptions');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ignored()
    {
        return view('users::cabinet.ignored');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function comments()
    {
        return view('users::cabinet.comments');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function saves()
    {
        return view('users::cabinet.saves');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notes()
    {
        return view('users::cabinet.notes');
    }
}