<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Post\Entities\Post;
use Modules\Post\Entities\Tag;

/**
 * Modules\Users\Entities\Community
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string|null $background
 * @property int $user_id
 * @property int $private
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $image_path
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Tag[] $tags
 * @property-read \Modules\Users\Entities\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Users\Entities\User[] $users
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereBackground($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community wherePrivate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Users\Entities\Community whereUserId($value)
 * @mixin \Eloquent
 */
class Community extends Model
{

    protected $fillable = ['name', 'slug', 'image', 'private'];

    protected $appends = ['show'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getShowAttribute()
    {
        return route('communities.show', $this->slug);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        return \Storage::url($value);
    }

    public function getImagePathAttribute()
    {
        return $this->attributes['image'];
    }

    public function scopeCommunitySearch($query, $search_string)
    {
        return $query->whereHas('user', function ($q) use ($search_string) {
            $q->where('name', 'like', '%'.$search_string.'%');
        })->orWhere('name', 'like', $search_string);
    }
}
