@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link @if(request('mode') === null || request('mode') === 'users') active @endif"
           href="{{ route('cabinet.subscriptions', ['users']) }}"
           title="Пользователи">Пользователи <span class="sr-only">(current)</span>
        </a>
        <div class="collapse navbar-collapse" id="navbarNav2">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'tags') active @endif"
                       href="{{ route('cabinet.subscriptions', ['tags']) }}"
                       title="Теги">Теги
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'communities') active @endif"
                       href="{{ route('cabinet.subscriptions', ['communities']) }}"
                       title="Сообщества">Сообщества
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    @if(request('mode') === null || request('mode') === 'users')
        <user-follow-list></user-follow-list>
    @elseif(request('mode') === 'tags')
        <tag-follow-list></tag-follow-list>
    @else
        <community-follow-list></community-follow-list>
    @endif
@stop