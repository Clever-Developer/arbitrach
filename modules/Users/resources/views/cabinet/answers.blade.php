@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link @if(request('mode') === null || request('mode') === 'all') active @endif" href="{{ route('cabinet.answers', ['all']) }}" title="Все посты">Все <span class="sr-only">(current)</span></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav3" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'posts') active @endif" href="{{ route('cabinet.answers', ['posts']) }}" title="тветы на посты">Ответы на посты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'comments') active @endif" href="{{ route('cabinet.answers', ['comments']) }}" title="Ответы на комментарии">Ответы на комментарии</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'mentioning') active @endif" href="{{ route('cabinet.answers', ['mentioning']) }}" title="Упоминания вас">Упоминания вас</a>
                </li>
            </ul>
        </div>
    </nav>

    <answers-list mode="{{ request('mode') }}" api-url="{{ route('api.comments.answers') }}"></answers-list>
@stop