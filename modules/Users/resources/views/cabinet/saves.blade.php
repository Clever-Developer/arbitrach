@extends('main::layouts.master')

@section('content')
    <nav class="navbar navbar-expand-lg">
        <a class="nav-link first-link @if(request('mode') === null || request('mode') === 'posts') active @endif"
           href="{{ route('cabinet.saves', ['posts']) }}"
           title="Посты">Посты <span class="sr-only">(current)</span>
        </a>
        <div class="collapse navbar-collapse" id="navbarNav3">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link @if(request('mode') === 'comments') active @endif"
                       href="{{ route('cabinet.saves', ['comments']) }}"
                       title="Комментарии">Комментарии
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    @if(request('mode') === null || request('mode') === 'posts')
        <post-list api-url="{{ route('api.saves.posts') }}">
            <post-filter slot="filter"></post-filter>
        </post-list>
    @else
        <user-comment-list
                api-url="{{ route('api.saves.comments') }}"
                mode="user"
        ></user-comment-list>
    @endif
@stop