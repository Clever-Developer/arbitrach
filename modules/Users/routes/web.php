<?php

use Illuminate\Support\Facades\Auth;

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Users\Http\Controllers'], function () {
    Auth::routes();

    Route::group(['middleware' => 'auth'], function () {

        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::put('update', 'UsersController@update');
            Route::post('search', 'UsersController@search');
            Route::get('{user}/{sort?}', 'UsersController@show')->name('show');
        });

        Route::group(['prefix' => 'cabinet', 'as' => 'cabinet.'], function () {
            Route::get('settings', 'CabinetController@settings')->name('settings');
            Route::get('votes', 'CabinetController@votes')->name('votes');
            Route::get('comments/{sort?}', 'CabinetController@comments')->name('comments');
            Route::get('answers/{mode?}', 'CabinetController@answers')->name('answers');
            Route::get('saves/{mode?}', 'CabinetController@saves')->name('saves');
            Route::get('communities', 'CabinetController@communities')->name('communities');
            Route::get('notes', 'CabinetController@notes')->name('notes');
            Route::get('subscriptions/{mode?}', 'CabinetController@subscriptions')->name('subscriptions');
            Route::get('ignored/{mode?}', 'CabinetController@ignored')->name('ignored');
            Route::get('/{sort?}', 'CabinetController@index')->name('index');
        });
    });
});
