<?php

namespace Modules\Post\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ChunkResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'content' => $this->content,
            'post_id' => $this->post_id,
            'type'    => $this->type,
            'order'   => $this->order
        ];
    }
}
