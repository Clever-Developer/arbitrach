<?php

namespace Modules\Post\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Modules\Users\Http\Resources\UserResource;

class CommentResource extends Resource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'post_id'     => $this->post_id,
            'user_id'     => $this->user_id,
            'parent_id'   => $this->parent_id,
            'rating'      => $this->rating,
            'text'        => $this->text,
            'image'       => $this->image ?? false,
            'commentable' => \Auth::check(),
            'replyable'   => \Auth::id() !== $this->user_id,
            'editable'    => \Auth::id() === $this->user_id && now()->subHour(1)->lte($this->created_at),
            'deletable'   => \Auth::id() === $this->user_id && now()->subHour(1)->lte($this->created_at),
            'saveable'    => $this->when(\Auth::check(), function () {
                return \Auth::user()->savedComments()->where('saveable_id', $this->id)->exists();
            }),
            'ago'         => \Jenssegers\Date\Date::parse($this->updated_at)->ago(),
            'date'        => optional($this->created_at)->format('dmyhis'),
            'user'        => $this->whenLoaded('user', function () {
                return UserResource::make($this->user);
            }),
            'parent'      => $this->whenLoaded('parent', function () {
                return CommentResource::make($this->parent);
            }),
            'childs'      => $this->whenLoaded('childs', function () {
                return CommentResource::collection($this->childs);
            }),
            'child'       => $this->whenLoaded('child', function () {
                return CommentResource::collection($this->child);
            }),
            'post'        => $this->whenLoaded('post', function () {
                return PostResource::make($this->post);
            })
        ];
    }
}
