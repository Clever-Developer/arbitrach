<?php

namespace Modules\Post\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'            => 'required|string|max:255',
            'slug'             => 'required|string|max:255|unique:posts,slug'.($this->id ? ','.$this->id : ''),
            'category_id'      => 'required|integer|exists:categories,id',
            'my'               => 'sometimes|boolean',
            'draft'            => 'sometimes|boolean',
            'chunks'           => 'required|array',
            'chunks.*.content' => 'required|string|max:2000',
            'chunks.*.type'    => 'required|string|max:255',
        ];
    }



    protected function prepareForValidation()
    {
        if ($slug = $this->get('title', false)) {
            $this->merge(['slug' => str_slug($slug)]);
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
