<?php

namespace Modules\Post\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Modules\Post\Entities\Category;
use Modules\Post\Entities\Chunk;
use Modules\Post\Entities\Post;
use Modules\Post\Http\Requests\DraftPostRequest;
use Modules\Post\Http\Requests\StorePostRequest;
use Modules\Post\Http\Requests\UpdatePostRequest;
use Modules\Post\Http\Resources\PostResource;

class PostController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeImage(Request $request)
    {
        $path = false;

        if ($file = $request->file('image')) {
            $path = $file->storeAs('public/tmp/'.\Auth::id(), $file->getClientOriginalName());
        }

        return response()->json(\Storage::url($path));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        $post->loadMissing(['chunks', 'tags', 'user', 'community']);

        $post = PostResource::make($post)->jsonSerialize();

        return view('post::show', compact('post'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post)
    {
        $categories = Category::prepared()->get();
        $post->loadMissing(['chunks', 'user', 'tags']);
        $post = PostResource::make($post)
            ->additional(['edit' => true])
            ->response()
            ->getContent();

        return view('post::create', compact('post', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $categories = Category::prepared()->get();
        $post       = \Auth::user()->posts()->where('draft', true)->first();

        if (! $post) {
            $post = \Auth::user()->posts()->create(['draft' => true]);
        }

        $post->loadMissing(['chunks', 'tags', 'community', 'user']);

        $post = PostResource::make($post)
            ->additional(['edit' => true])
            ->response()
            ->getContent();

        return view('post::create', compact('categories', 'post'));
    }

    /**
     * @param StorePostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StorePostRequest $request)
    {
        $post = $this->storeFromRequest($request);

        $post->update(['draft' => false]);

        return response()->json([
            'success' => (bool)$post,
            'url'     => route('posts.show', ['slug' => $post->slug])
        ]);
    }

    /**
     * @param DraftPostRequest $request
     * @return PostResource
     */
    public function draft(DraftPostRequest $request)
    {
        $post = $this->storeFromRequest($request);

        $post->update(['draft' => true]);
        $post->loadMissing(['chunks', 'tags', 'community', 'user']);

        return PostResource::make($post)->additional([
            'updated_at' => $post->updated_at->format('h:i:s'),
            'edit'       => true
        ]);
    }

    /**
     * @param UpdatePostRequest $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $result = $post->update($request->all());

        if ($chunks = $request->get('chunks', [])) {
            foreach ($chunks as $key => $chunk) {
                if (isset($chunk['id'])) {
                    $chunk['order'] = $key + 1;

                    if ($chunk['type'] === Chunk::$TYPE_IMAGE) {
                        unset($chunk['content']);
                    }

                    $post->chunks()->find($chunk['id'])->update($chunk);
                } else {
                    if ($chunk['type'] === Chunk::$TYPE_IMAGE) {
                        $new = 'public/posts/'.$post->id.'/'.Str::random(10).'.jpg';
                        \Storage::move('public/'.str_replace_first('storage', '', $chunk['content']), $new);
                        $chunk['content'] = $new;
                    }

                    $post->chunks()->create([
                        'content' => $chunk['content'],
                        'type'    => $chunk['type'],
                        'order'   => $key + 1,
                    ]);
                }
            }
        }

        if ($tags = $request->get('tags', [])) {
            if (! empty($tags)) {
                $post->tags()->sync(collect($tags)->pluck('id'));
            }
        }

        return response()->json([
            'success' => $result,
            'url'     => route('posts.edit', ['slug' => $post->slug])
        ]);
    }

    /**
     * @param Chunk $chunk
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteChunk(Chunk $chunk)
    {
        if ($chunk->type === Chunk::$TYPE_IMAGE) {
            \Storage::delete($chunk->imagePath);
        }

        $result = $chunk->delete();

        return response()->json(['success' => $result]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|null|object
     */
    private function storeFromRequest(Request $request)
    {
        $post = \Auth::user()->posts()->where('draft', true)->first();

        if (! $post) {
            $post = \Auth::user()->posts()->create($request->all());
        } else {
            $post->update($request->all());
        }

        if ($chunks = $request->get('chunks', [])) {
            foreach ($chunks as $key => $chunk) {
                if (isset($chunk['id'])) {
                    $chunk['order'] = $key + 1;

                    if ($chunk['type'] === Chunk::$TYPE_IMAGE) {
                        unset($chunk['content']);
                    }

                    $post->chunks()->find($chunk['id'])->update($chunk);
                } else {
                    if ($chunk['type'] === Chunk::$TYPE_IMAGE) {
                        $new = 'public/posts/'.$post->id.'/'.Str::random(10).'.jpg';
                        \Storage::move('public/'.str_replace_first('storage', '', $chunk['content']), $new);
                        $chunk['content'] = $new;
                    }

                    $post->chunks()->create([
                        'content' => $chunk['content'],
                        'type'    => $chunk['type'],
                        'order'   => $key + 1,
                    ]);
                }
            }
        }

        if ($tags = $request->get('tags', [])) {
            if (! empty($tags)) {
                $post->tags()->sync(collect($tags)->pluck('id'));
            }
        }

        return $post;
    }
}
