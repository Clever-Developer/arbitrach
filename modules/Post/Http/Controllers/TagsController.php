<?php

namespace Modules\Post\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Post\Entities\Tag;

class TagsController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tags = Tag::prepared()->withCount('posts')->orderByDesc('posts_count')->get();

        return view('post::tags.index', compact('tags'));
    }

    /**
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Tag $tag)
    {
        return view('post::tags.show', compact('tag'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search = $request->get('search', false);
        $tags   = Tag::prepared()->withCount('posts')->orderByDesc('posts_count');

        if ($search) {
            $tags = $tags->where('name', 'like', "%$search%");
        }

        $tags = $tags->get();

        return view('post::tags.partials.list', compact('tags'));
    }
}
