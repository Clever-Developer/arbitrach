<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Modules\Main\Entities\Menu;

/**
 * Modules\Post\Entities\Category
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $order
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Main\Entities\Menu[] $menus
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Post[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Post[] $preparedPosts
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category prepared()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Category extends Model
{

    protected $fillable = [
        'name',
        'slug',
        'parent_id',
        'order',
        'active'
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function menus()
    {
        return $this->belongsToMany(Menu::class);
    }

    /**
     * @return mixed
     */
    public function preparedPosts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @param $query
     */
    public function scopePrepared($query)
    {
        /** @var Builder $query */
        $query->where('active', true)
            ->orderBy('order');
    }
}
