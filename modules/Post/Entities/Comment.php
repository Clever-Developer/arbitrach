<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\User;

/**
 * Modules\Post\Entities\Comment
 *
 * @property int $id
 * @property int $post_id
 * @property int $user_id
 * @property int|null $parent_id
 * @property int $rating
 * @property string $text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Comment[] $childs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Post\Entities\Like[] $likes
 * @property-read \Modules\Post\Entities\Comment|null $parent
 * @property-read \Modules\Post\Entities\Post $post
 * @property-read \Modules\Users\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Comment whereUserId($value)
 * @mixin \Eloquent
 */
class Comment extends Model
{

    protected $guarded = ['id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Comment::class, 'parent_id')->with(['user', 'childs']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child()
    {
        return $this->hasMany(Comment::class, 'parent_id')->with('user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Comment::class, 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @param $query
     * @param $search_string
     * @return mixed
     */
    public function scopeCommentsSearch($query, $search_string)
    {
        return $query->whereHas('user', function ($q) use ($search_string) {
            $q->where('name', 'like', '%'.$search_string.'%');
        })->orWhere('text', 'like', $search_string);
    }

    /**
     * @param $value
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value ? \Storage::url($value) : null;
    }

    public function getImagePathAttribute()
    {
        return $this->attributes['image'];
    }
}
