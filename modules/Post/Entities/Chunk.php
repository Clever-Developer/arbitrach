<?php

namespace Modules\Post\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Post\Entities\Chunk
 *
 * @property int $id
 * @property string $content
 * @property int $post_id
 * @property int $order
 * @property string $type
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Modules\Post\Entities\Post $post
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Post\Entities\Chunk whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $image_path
 */
class Chunk extends Model
{

    public static $TYPE_TEXT  = 'text';
    public static $TYPE_IMAGE = 'image';
    public static $TYPE_VIDEO = 'video';

    protected $fillable = ['content', 'type', 'post_id', 'order'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getContentAttribute($value)
    {
        if ($this->type === self::$TYPE_IMAGE) {
            return \Storage::url($value);
        }
        return $value;
    }

    public function getImagePathAttribute()
    {
        return $this->attributes['content'];
    }
}
