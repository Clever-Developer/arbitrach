<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('slug')->unique()->nullable();
            $table->string('title')->nullable();

            $table->unsignedInteger('category_id')->nullable();
            $table->unsignedInteger('community_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->integer('rating')->default(0);

            $table->boolean('my')->default(false);
            $table->boolean('draft')->default(false);
            $table->boolean('erotic')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
