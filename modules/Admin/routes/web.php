<?php

Route::group(['middleware' => ['web'], 'namespace' => 'Modules\Admin\Http\Controllers', 'prefix' => 'admin'], function () {

    Route::group(['middleware' => ['web']],
        function () {
            Route::middleware(['checkAdmin', 'counts'])->group(function () {
                Route::get('', 'AdminController@index')->name('adminsHome');
                Route::get('users/search', 'UsersController@search')->name('admin.users.search');
                Route::get('categories/search', 'CategoriesController@search')->name('admin.categories.search');
                Route::get('comments/search', 'CommentsController@search')->name('admin.comments.search');
                Route::get('communities/search', 'CommunitiesController@search')->name('admin.communities.search');
                Route::get('menus/search', 'MenusController@search')->name('admin.menus.search');
                Route::get('posts/search', 'PostsController@search')->name('admin.posts.search');
                Route::get('tags/search', 'TagsController@search')->name('admin.tags.search');

                Route::resource('menus', 'MenusController', ['as' => 'admin']);
                Route::resource('categories', 'CategoriesController', ['as' => 'admin']);
                Route::resource('users', 'UsersController', ['as' => 'admin']);
                Route::resource('tags', 'TagsController', ['as' => 'admin']);
                Route::resource('comments', 'CommentsController', ['as' => 'admin']);
                Route::resource('posts', 'PostsController', ['as' => 'admin']);

                Route::resource('communities', 'CommunitiesController', ['as' => 'admin']);
                Route::put('updateCommunityImage', 'CommunitiesController@changeImage');
                Route::put('updateCommentImage', 'CommentsController@changeImage');

                Route::post('tagsCheckbox/change', 'TagsController@tagsChange');
                Route::post('communitiesCheckbox/change', 'CommunitiesController@communitiesChange');
                Route::post('postsCheckbox/change', 'PostsController@postsChange');
                Route::post('categoryCheckbox/change', 'CommunitiesController@communityChange');
                Route::post('TagCheckbox/change', 'TagsController@tagsTagChange');
                Route::post('CategoryPostsCheckbox/change', 'CategoriesController@catsPostChange');

                Route::post('CommunityChangeTagsCheckbox/change', 'TagsController@communityChangeTagsCheck');
                Route::post('CommunityChangeUsersCheckbox/change', 'UsersController@communityChangeUsersCheck');
                Route::post('CommunityChangePostsCheckbox/change', 'PostsController@communityChangePostsCheck');
            });
        });
});
