<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
//use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\CategoryCreateRequest;
use Modules\Admin\Http\Requests\CategoryUpdateRequest;
use Modules\Main\Entities\Menu;
use Modules\Post\Entities\Category;
use Modules\Post\Entities\Post;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categories = Category::orderBy('order')->paginate(4);

        return view('admin::modules.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Category $category)
    {
        return view('admin::modules.categories.item', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CategoryCreateRequest $request)
    {
        if ($request->filled('slug')) {
            $slug = $request->input('slug');
        } else {
            $slug = str_slug($request->input('name'));
        }

        Category::create([
            'name'   => $request->name,
            'order'  => $request->order,
            'slug'   => $slug,
            'active' => $request->active
        ]);

        return redirect(route('admin.categories.index'));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Category $category)
    {
        $menusCollection = Menu::all();
        $menus           = parent::dataSome($menusCollection, "menus", $category, 'belongsToMany');

        $postsCollection = Post::all();
        $posts           = parent::dataSome($postsCollection, "posts", $category, 'hasMany', 'category_id');

        $CategoryPostsUrl = '/admin/CategoryPostsCheckbox/change';

        return view('admin::modules.categories.item', compact('category', 'menus', 'posts'), [
            'CategoryPostsUrl' => $CategoryPostsUrl
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        if ($request->filled('slug')) {
            $slug = $request->input('slug');
        } else {
            $slug = str_slug($request->input('name'));
        }

        $category->update([
            'name'   => $request->name,
            'order'  => $request->order,
            'slug'   => $slug,
            'active' => $request->active
        ]);

        $category->menus()->sync($request->checked);

        //$category->posts()


        return redirect(route('admin.categories.index'));
    }

    public function destroy(Request $request)
    {
        $id = intval($request->id);

        if ($id == 0) {
            return redirect()->back()->withErrors('Ой');
        }

        Post::where('category_id', $id)->update(['category_id' => 0]);

        $category = Category::find($id);
        $category->menus()->sync([]);
        $category->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $search_string = '%'.$request->get('search').'%';

        $categories = Category::where('name', 'like', $search_string)
            ->orderByDesc('id')
            ->paginate(10);

        return view('admin::modules.categories.index', compact('categories'));
    }

    public function catsPostChange(Request $request)
    {
        $category_id = $request->f_id;
        $post_id     = $request->l_id;

        $category = Category::find($category_id);
        $post     = Post::find($post_id);

        if ($request->parametr == 'true') {
            $post->category_id = $category_id;
        } elseif ($request->parametr == 'false') {
            $post->category_id = 0;
        }
        $post->save();

        return response()->json('ok');
    }
}
