<?php

namespace Modules\Admin\Http\Controllers;


use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    //highly useful method, if we need to show checked data at first in model relations
    public function dataSome($collection, $relation, $model, $relationType = '', $field = '')
    {
        $first  = [];
        $second = [];

        $atLast = [];

        if (($relationType == 'belongsToMany') or ($relationType == '')) {
            foreach ($collection as $key) {
                if (count($model->$relation) > 0) {
                    if (in_array($key->id, $model->$relation->pluck('id')->toArray())) {
                        $first[] = $key;
                    } else {
                        $second[] = $key;
                    }
                } else {
                    $atLast[] = $key;
                }
            }
        } elseif ($relationType == 'hasMany') {
            foreach ($collection as $key) {
                if ($model->$relation) {
                    if (count((array)$model->$relation) > 0) {
                        if ($key->$field == $model->id) {
                            $first[] = $key;
                        } else {
                            $second[] = $key;
                        }
                    } else {
                        $atLast[] = $key;
                    }
                } else {
                    $atLast[] = $key;
                }
            }
        }

        if (count($atLast) > 0) {
            return collect($atLast);
        } else {
            return collect(array_merge($first, $second));
        }
    }
}
