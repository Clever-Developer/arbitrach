<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name'  => 'sometimes|string',
                'slug'  => 'max:255|unique:posts,slug'.($this->id ? ",$this->id" : '')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->request->set('draft', $this->request->has('draft') && $this->request->get('draft') === 'on');
        $this->request->set('my', $this->request->has('my') && $this->request->get('my') === 'on');
        $this->request->set('publish', $this->request->has('publish') && $this->request->get('publish') === 'on');
    }
}
