<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('/assets/img/favicon.png') }}">
    <title>Admin Panel | Arbitrach</title>

    <!-- Icons -->
    <link href="{{ asset('assets/vendors/css/flag-icon.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/simple-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/css/jquery.datetimepicker.min.css') }}" rel="stylesheet">


    <!-- Main styles for this application -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/admin.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/datatTablePagin.css') }}" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a href="/admin" style="padding-left: 15px"><img src="{{ asset('image/logo.svg') }}"></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
        <span class="navbar-toggler-icon"></span>
    </button>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" href="/">На сайт</a>
        </li>
    </ul>

    <ul class="nav navbar-nav" style="margin-right: 40px;margin-left: 25px;">
        <li class="nav-item dropdown">
            <a class="nav-link" href="#" onclick="document.getElementById('logout').submit()">Выйти</a>
            <form action="{{ route('logout') }}" method="POST" style="display: none;" id="logout">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>

</header>
<div class="app-body">

    <div class="sidebar">
        <nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.users.index') }}">
                        <i class="fa fa-user-circle-o"></i>
                        Пользователи
                        <span class="badge badge-info">{{ $users_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.tags.index') }}">
                        <i class="fa fa-tags"></i>
                        Теги
                        <span class="badge badge-info">{{ $tags_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.categories.index') }}">
                        <i class="fa fa-pie-chart"></i>
                        Категории
                        <span class="badge badge-info">{{ $category_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.menus.index') }}">
                        <i class="fa fa-sitemap"></i>
                        Пункты Меню
                        <span class="badge badge-info">{{ $menu_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.comments.index') }}">
                        <i class="fa fa-comments-o"></i>
                        Комментарии
                        <span class="badge badge-info">{{ $comment_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.posts.index') }}">
                        <i class="fa fa-sticky-note-o"></i>
                        Посты
                        <span class="badge badge-info">{{ $post_count }}</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin.communities.index') }}">
                        <i class="fa fa-group"></i>
                        Сообщества
                        <span class="badge badge-info">{{ $community_count }}</span>
                    </a>
                </li>
            </ul>
        </nav>
        <button class="sidebar-minimizer brand-minimizer" type="button"></button>
    </div>

    <!-- Main content -->
    <main class="main">

        <br>

        <div class="container-fluid">

            @yield('content')

        </div>
    </main>

</div>

{{--
<footer class="app-footer">--}} {{--
    <span><a href="http://coreui.io">CoreUI</a> &copy; 2018 creativeLabs.</span>--}} {{--
    <span class="ml-auto">Powered by <a href="http://coreui.io">CoreUI</a></span>--}} {{--
  </footer>--}}

<!-- Bootstrap and necessary plugins -->
<script src="{{ asset('assets/vendors/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/pace.min.js') }}"></script>
<script src="{{ asset('assets/vendors/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/vendors/js/jquery.datetimepicker.full.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/admin.js') }}"></script>
<script src="{{ asset('js/datatablesjquery.js') }}"></script>

<!-- CoreUI main scripts -->

<script src="{{ asset('assets/js/app.js') }}"></script>

@stack('js')


</body>

</html>
