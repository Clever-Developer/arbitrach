<div class="col-sm-12 profile">
    <div class="row card">
        <div class="card-header" style="display: flex; flex-direction: row;justify-content: space-between;">
            <h4 style="margin: 0;    padding-top: 2px;">Комментарии</h4>
        </div>
        <div class="table-responsive">
            <table class="table" style="margin-bottom:0;">
                <tr>
                    <th>№</th>
                    <th>Рейтинг</th>
                    <th>Автор</th>
                    <th>Текст</th>
                    <th>Добавлен</th>
                    <th>Действия</th>
                </tr>
                @foreach ($comments as $key)
                    <tr>
                        <td>
                            <strong class="badge">
                                <a href="{{ route('admin.comments.edit', ['id' => $key->id]) }}" target="_blank">
                                    @if($key->image)
                                    <img src="{{ asset($key->image) }}" class="img-rounded" style="margin-right:10px; max-width:16px; max-height:16px;">
                                    @endif
                                    {{ $loop->iteration }}
                                </a>
                            </strong>
                        </td>
                        <td>{{ $key->rating }}</td>
                        <td>@if($key->user) {{ $key->user->name }} @else - @endif</td>
                        <td>{{ str_limit($key->text, $limit = 10, $end = '...') }}</td>
                        <td>{{ $key->created_at }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <a href="{{ route('admin.comments.edit', ['id' => $key->id]) }}"
                                   class="btn btn-sm btn-success">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <form action="{{ route('admin.comments.destroy', $key->id) }}" method="post" style="margin-left:5px !important;">
                                    @csrf
                                    @method('delete')
                                    <input type="hidden" name="id" value="{{ $key->id }}">
                                    <button type="submit" class="btn btn-sm btn-danger"><i
                                                class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>