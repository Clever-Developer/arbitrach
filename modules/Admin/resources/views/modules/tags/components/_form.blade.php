<form action="{{ $tag->id ? route('admin.tags.update', $tag->slug) : route('admin.tags.store')}}"
      method="POST">
    @csrf
    @if($tag->id)
        @method('put')
    @endif
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if($tag->id)
                <div class="row form-group">
                    <strong>Id</strong>
                    <input class="form-control" type="text" value="{{ $tag->id }}" disabled>
                </div>
            @endif
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="name" class="form-control" type="text" value="{{ $tag->name }}">
            </div>
            <div class="row form-group">
                <strong>Порядок</strong>
                <input name="order" class="form-control" type="text" value="{{ $tag->order }}">
            </div>
            <div class="row form-group">
                <strong>Путь</strong>
                <input name="slug" class="form-control" type="text" value="{{ $tag->slug }}">
            </div>
            <div class="row form-check">
                <label>
                    <input name="active" class="form-check-input" type="checkbox" @if($tag->active) checked @endif>
                    <strong>Активный</strong>
                </label>
            </div>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample3"
                    aria-expanded="false" aria-controls="collapseExample3" style="width: 100% !important;">
                Пользователи
            </button>
            <br>
            <hr>
            <div class="collapse" id="collapseExample3">
                <div class="card card-body">
                    @if(count($tag->users)==0)
                        <h4 class="text-center">Ничего не найдено</h4>
                    @else
                    <div class="table-responsive">
                        <table class="tabulka_kariet table table-bordered table-hover">
                            <thead>
                            <tr class="alert alert-secondary" id="loadAjaxTagUsers" style="display:none">
                                <td colspan="2" style="width:100%; text-align:center"><b><i
                                                class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                                </td>
                            </tr>
                            <tr>
                                <th>Наименование</th>
                                <th>Отношение</th>
                            </tr>
                            </thead>
                            @foreach($users as $key)
                                @if(count($tag->users)>0)
                                        <tr>
                                            <td>{{ $key->name }}</td>
                                            <td style='text-align:center'>
                                                <div class="form-check">
                                                    <input class="form-check-input"
                                                           type="checkbox"
                                                           id="defaultCheckkkkkk{{ $key->id }}"
                                                           @if(count($tag->users)>0)
                                                           @if(in_array($key->id, $tag->users->pluck('id')->toArray()))
                                                           checked
                                                           @endif
                                                           @endif
                                                           onchange="CheckSwitch({{ $tag->id }},{{ $key->id }},'loadAjaxTagUsers',{{ json_encode($tagsTagUrl) }},this)">
                                                </div>
                                            </td>
                                        </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row form-group">
                @if($tag->id)
                    <input type="hidden" name="id" value="{{ $tag->id }}">
                @else
                @endif
                <button class="btn btn-success btn-block" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</form>

