<form method="GET" action="{{ route('admin.tags.search') }}" class="form-group row" style="margin-bottom: 0">
    <div class="col-md-12">
        <div class="input-group">
            <input type="text" class="form-control" name="search"
                   value="{{ Request::get('search') }}" placeholder="Введите строку для поиска">
            <span class="input-group-append">
                <button type="submit" class="btn btn-primary" style="margin-top: 0;">Найти</button>
            </span>
        </div>
    </div>
</form>