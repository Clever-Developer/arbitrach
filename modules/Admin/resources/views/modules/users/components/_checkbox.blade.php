<div class="row form-check">
    <label>
        <input style="" name="{{ $field }}" class="form-check-input" type="checkbox" @if($user->{$field}) checked @endif>
        <strong>{{ $title }}</strong>
    </label>
</div>

