@extends('admin::layouts.master')

@section('content')
    <div class="col-sm-12 profile">
        <div class="row card">
            <div class="card-header">
                <strong>Категория</strong>
            </div>
            <div class="card-body">
                @include('admin::modules.categories.components._form')
            </div>
        </div>
    </div>
    <style>
        .profile .card-body .col-3 img {
            height: 300px;
            max-width: 100%;
        }
    </style>
@endsection