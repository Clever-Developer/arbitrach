<table>
        <tr>
            <td>
                @include('admin::modules.communities.components.image')
            </td>
            <td style="width: 150%;">
                <form action="{{ route('admin.communities.update', $community->slug) }}"
                      method="POST">
                    @csrf
                    @method('put')
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row form-group">
                <strong>Id</strong>
                <input class="form-control" type="text" value="{{ $community->id }}" disabled>
            </div>
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="name" class="form-control" type="text" value="{{ $community->name }}">
            </div>
            <div class="row form-group">
                <strong>Путь</strong>
                <input name="slug" class="form-control" type="text" value="{{ $community->slug }}">
            </div>
        </div>
        <div class="col-md-8 offset-2">
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample3"
                  aria-expanded="false" aria-controls="collapseExample3" style="width: 100% !important;">
              Теги
          </button>
          <br>
          <hr>
          <div class="collapse" id="collapseExample3">
              <div class="card card-body">
                  @if(count($community->tags)==0)
                      <h4 class="text-center">Ничего не найдено</h4>
                  @endif
                  <div class="table-responsive">
                      <table class="tabulka_kariet table table-bordered table-hover">
                          <thead>
                          <tr class="alert alert-secondary" id="loadAjaxTagUsersCommunity" style="display:none">
                              <td colspan="2" style="width:100%; text-align:center"><b><i
                                              class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                              </td>
                          </tr>
                          <tr>
                              <th>Наименование</th>
                              <th>Отношение</th>
                          </tr>
                          </thead>
                          @foreach($tags as $key)
                              <tr>
                                  <td>{{ $key->name }}</td>
                                  <td style='text-align:center'>
                                      <div class="form-check">
                                          <input class="form-check-input"
                                                 type="checkbox"
                                                 id="defaultCheckkkkkkk{{ $key->id }}"
                                                 @if(count($community->tags)>0)
                                                 {{ in_array($key->id, $community->tags->pluck('id')->toArray()) ? 'checked' : '' }}
                                                 @endif
                                                 onchange="CheckSwitch({{ $community->id }},{{ $key->id }},'loadAjaxTagUsersCommunity',{{ json_encode($CommunityChangeTagsURL) }},this)">
                                      </div>
                                  </td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              </div>
          </div>
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample4"
                  aria-expanded="false" aria-controls="collapseExampl43" style="width: 100% !important;">
              Подписчики
          </button>
          <br>
          <hr>
          <div class="collapse" id="collapseExample4">
              <div class="card card-body">
                  @if(count($community->users)==0)
                      <h4 class="text-center">Ничего не найдено</h4>
                  @endif
                  <div class="table-responsive">
                      <table class="tabulka_kariet table table-bordered table-hover">
                          <thead>
                          <tr class="alert alert-secondary" id="loadAjaxUsersCommunitySome" style="display:none">
                              <td colspan="2" style="width:100%; text-align:center"><b><i
                                              class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                              </td>
                          </tr>
                          <tr>
                              <th>Наименование</th>
                              <th>Отношение</th>
                          </tr>
                          </thead>
                          @foreach($users as $key)
                              <tr>
                                  <td>{{ $key->name }}</td>
                                  <td style='text-align:center'>
                                      <div class="form-check">
                                          <input class="form-check-input"
                                                 type="checkbox"
                                                 id="defaultCheckkkkkqkk{{ $key->id }}"
                                                 @if(count($community->users)>0)
                                                 {{ in_array($key->id, $community->users->pluck('id')->toArray()) ? 'checked' : '' }}
                                                 @endif
                                                 onchange="CheckSwitch({{ $community->id }},{{ $key->id }},'loadAjaxUsersCommunitySome',{{ json_encode($CommunityChangeUsersURL) }},this)">
                                      </div>
                                  </td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              </div>
          </div>
          <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample6"
                  aria-expanded="false" aria-controls="collapseExample6" style="width: 100% !important;">
              Посты
          </button>
          <br>
          <hr>
          <div class="collapse" id="collapseExample6">
              <div class="card card-body">
                  @if(count($community->posts)==0)
                      <h4 class="text-center">Ничего не найдено</h4>
                  @endif
                  <div class="table-responsive">
                      <table class="tabulka_kariet table table-bordered table-hover">
                          <thead>
                          <tr class="alert alert-secondary" id="loadAjaxCommunitySomePosts" style="display:none">
                              <td colspan="2" style="width:100%; text-align:center"><b><i
                                              class="fa fa-spinner fa-spin" style="font-size:20px"></i></b>
                              </td>
                          </tr>
                          <tr>
                              <th>Наименование</th>
                              <th>Отношение</th>
                          </tr>
                          </thead>
                          @foreach($posts as $key)
                              <tr>
                                  <td>{{ $key->title }}</td>
                                  <td style='text-align:center'>
                                      <div class="form-check">
                                          <input class="form-check-input"
                                                 type="checkbox"
                                                 id="defaultCheckkkkkk{{ $key->id }}"
                                                 @if(count($community->posts)>0)
                                                 @if($key->community_id == $community->id)
                                                 checked
                                                 @endif
                                                 @endif
                                                 onchange="CheckSwitch({{ $community->id }},{{ $key->id }},'loadAjaxCommunitySomePosts',{{ json_encode($CommunityChangePostsURL) }},this)">
                                      </div>
                                  </td>
                              </tr>
                          @endforeach
                      </table>
                  </div>
              </div>
          </div>
              <button class="btn btn-success btn-block" type="submit">Сохранить</button>
</div>
</div>
      </form>
  </td>
</tr>
</table>
