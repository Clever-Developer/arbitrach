<div class="row form-group">
    <div>
        <img id="communityImage" name="image" src="{{ asset($community->image) }}"
             style="width: 150px; height: 150px; margin-left: 100px;">
    </div>
    <table style="margin-left: 100px;">
        <tr>
            <td>
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-pencil"></i>
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Изменить изображение</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form id="changeFormImage" method="POST" action="{{ URL::to('/admin/updateCommunityImage') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <div class="form-group">
                                        <div class="input-group">
                                       <span class="input-group-btn">
                               <span class="btn btn-default btn-file">
                                   <input type="hidden" name="community_id" value="{{ $community->id }}">
                                           <input type="file" name="file" accept="image/*" required>
                                   </span>
                                 </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button"
                                        class="btn btn-primary"
                                        onclick="document.getElementById('changeFormImage').submit();">Сохранить</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>