<form action="{{ route('admin.posts.update', $post->slug) }}"
      method="POST">
    @csrf
    @method('put')
    <div class="row">
        <div class="col-md-8 offset-2">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul style="list-style-type: none">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row form-group">
                <strong>Id</strong>
                <input class="form-control" type="text" value="{{ $post->id }}" disabled>
            </div>
            <div class="row form-group">
                <strong>Имя</strong>
                <input name="title" class="form-control" type="text" value="{{ $post->title }}">
            </div>
            <div class="row form-group">
                <strong>Путь</strong>
                <input name="slug" class="form-control" type="text" value="{{ $post->slug }}">
            </div>
            <label>
                <input name="draft" class="form-check-input" type="checkbox"
                       @if($post->draft) checked @endif>
                <strong>Черновик</strong>
            </label>
            <br>
            <label>
                <input name="my" class="form-check-input" type="checkbox"
                       @if($post->my) checked @endif>
                <strong>Моё</strong>
            </label>
                <hr>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                    data-target="#collapseExample"
                    aria-expanded="false" aria-controls="collapseExample" style="width: 100% !important;">
                Категории
            </button>
            <br>
            <hr>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <ul class="list-group w-200">
                        <li class="list-group-item">
                            <div class="list-group-item list-group-item-action">
                                <div class="form-check text-center">
                                    <div class="form-check">
                                        <div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   name="unchecked"
                                                   id="exampleRadios2"
                                                   @if($post->category_id==0)
                                                   checked
                                                    @endif>
                                            <label class="form-check-radio" for="exampleRadios2">
                                                Ничего
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @foreach($categories as $key)
                            <li class="list-group-item">
                                <div class="list-group-item list-group-item-action">
                                    <div class="form-check text-center">
                                        <div class="form-check">
                                            <div class="form-check">
                                                <input class="form-check-input"
                                                       type="radio"
                                                       name="checked"
                                                       id="exampleRadios2"
                                                       @if($key->id == $post->category_id)
                                                       checked
                                                       @endif
                                                       value="{{ $key->id }}">
                                                <label class="form-check-label" for="exampleRadios2">
                                                    {{ $key->name }}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="row form-group">
                <input type="hidden" name="id" value="{{ $post->id }}">
                <button class="btn btn-success btn-block" type="submit">Сохранить</button>
            </div>
        </div>
    </div>
</form>